/*-----------------------------------------------------------------------------
This template demonstrates how to use an IntentDialog with a LuisRecognizer to add 
natural language support to a bot. 
For a complete walkthrough of creating this type of bot see the article at
https://aka.ms/abs-node-luis
-----------------------------------------------------------------------------*/
'use strict';
var builder = require('botbuilder');
var botbuilder_azure = require('botbuilder-azure');
var path = require('path');
var config = require('./config');
var useEmulator = (process.env.NODE_ENV == 'development');
var redis = require("redis").createClient(config.redisOptions);
var imageService = require('./image-service');

var msAppConfig = {
  appId: config.appId,
  appPassword: config.appPassword,
  stateEndpoint: config.stateEndpoint,
  openIdMetadata: config.openIdMetaData,
};

var connector = useEmulator
  ? new builder.ChatConnector(msAppConfig)
  : new botbuilder_azure.BotServiceConnector(msAppConfig);

var bot = new builder.UniversalBot(connector);
bot.localePath(path.join(__dirname, './locale'));

// Make sure you add code to validate these fields
var luisAppId = config.luisAppId;
var luisAPIKey = config.luisApiKey;
var luisAPIHostName = config.luisApiHostName;

const LuisModelUrl = 'https://' + luisAPIHostName + '/luis/v1/application?id=' + luisAppId + '&subscription-key=' + luisAPIKey;

// Main dialog with LUIS
var recognizer = new builder.LuisRecognizer(LuisModelUrl);
var intents = new builder.IntentDialog({ recognizers: [recognizer] })
/*
.matches('<yourIntent>')... See details at http://docs.botframework.com/builder/node/guides/understanding-natural-language/
*/
  .matches('Greetings', [
    (session) => {
      session.send(
        randomFromArray(['Hi!', 'Hey there!', 'Howdy!', 'Greetings.'])
        + ' How may I help you today?'
      );
    },
  ])
  .matches('report_light_issue', [
    (session, args) => {
      console.log(JSON.stringify(args, null, 2));
      //session.send(randomFromArray(['I\'m sorry to hear that, may I ask where it is?']));

      const entitiesMap = args.entities.reduce((map, entity) => {
        map[entity.type] = entity;
        return map;
      }, {});

      session.send('Thanks! I am going file a case right now for you and please wait for a moment.');

      createNewCase(session.message.user, entitiesMap.malfunction.entity, entitiesMap.utility.entity, entitiesMap.location.entity)
        .then((newCase) => {
          redis.lpush('cases', JSON.stringify(newCase), (err, obj) => {
            session.send('Case has been filed and your case number is ' + newCase.id + '. You can track your latest update of your case anytime from here and please accept my apologies for any inconvenience caused.');
          });
        })
        .catch((error) => {
          session.send('Sorry, something went wrong when communicating to the service; Please try again later.');
        });
    },
  ])
  .onDefault((session) => {
    if (session.message.attachments) {
      //console.log(session.message.attachments[0]);
      //session.send(JSON.stringify(session.message, null, 2));
      //session.send('ok');
      imageService.getTagsFromUrl('https://scontent.xx.fbcdn.net/v/t35.0-12/22712858_10159517154960707_1510776628_o.jpg?_nc_ad=z-m&_nc_cid=0&oh=ca10047d1569de3b887c23336d283264&oe=59EE6D15')
        .then((tags) => {
          if (tags.some(tag => tag === 'light')) {
            bot.beginDialog(session.message.address, 'report_light_issue_from_image');
          }
        })
        .catch(console.error);

      return;
    }

    session.send('Sorry, I did not understand \'%s\'.', session.message.text);
  });

bot.dialog('/', intents);

bot.dialog('report_light_issue_from_image', [
  (session, args, next) => {
    session.send('I see that there is a light. Is there something wrong with it?');
    session.endDialog();
  },
]);

if (useEmulator) {
  var restify = require('restify');
  var server = restify.createServer();
  server.listen(3978, function() {
    console.log('test bot endpont at http://localhost:3978/api/messages');
  });
  server.post('/api/messages', connector.listen());
} else {
  module.exports = { default: connector.listen() }
}

function debug(o, session) {
  session.send(JSON.stringify(o, null, 2));
}

function randomFromArray(options) {
  return options[Math.floor(Math.random() * options.length)];
}

function createNewCase(user, type, object, location) {
  if (!user) {
    user = {
      id: 0,
      name: "System default",
    };
  }

  if (/flat/.test(location)) {
    switch (user.name) {
      case 'Justin Lau':
        location = '5A';
        break;
    }
  }

  return new Promise((resolve) => {
    const now = new Date();
    redis.incr('id_increment', (err, lastId = 10000) => {
      resolve({
        id: lastId,
        user,
        type,
        object,
        location,
        workflow_status: 'opened',
        created_at: now,
        updated_at: now,
      });
    });
  });
}
